import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

/* You need the context to live outside the function because react-responsive
   copies the reference to matchMedia so you can't change it later. You can
   change context though! :) */
const context = { matcher: () => false };
window.matchMedia =(query)  => ({
    matches: context.matcher(query),
    addListener: function() {},
    removeListener: function() {},
});

/* Only required for the latest enzyme */
Enzyme.configure({ adapter: new Adapter() });

/* This require statement is important! Import statements are hoisted to the top.
   require statements happen where they're written. It's why mocking wasn't working */
const responsive = require('./responsive').default;

const Desktop = () => <div id="desktop" />;
const Mobile = () => <div id="mobile" />;

/* The Magic */
const Responsive = responsive({
  Desktop,
  Mobile,
});



describe('when no matches', () => {
  beforeEach(() => {
    context.matcher = (query) => false;
  });
  it('renders desktop component', () => {
    const wrapper = mount(<Responsive />);
    expect(wrapper.find('#desktop').length).toBe(1);
  });
})
  

describe('when mobile', () => {
  beforeEach(() => {
    /* Only match "Mobile" */
    context.matcher = (query) => query === '(max-width: 767px)';
  });

  it('renders mobile component', () => {
    const wrapper = mount(<Responsive />);
    expect(wrapper.find('#mobile').length).toBe(1);
  });

  it('does not render desktop component', () => {
    const wrapper = mount(<Responsive />);
    expect(wrapper.find('#desktop').length).toBe(0);
  });
});

describe('when desktop', () => {
  beforeEach(() => {
    /* Only match "Desktop" */
    context.matcher = (query) => query === '(min-width: 768px)'; 
  });

  it('renders desktop component', () => {
    const wrapper = mount(<Responsive />);
    expect(wrapper.find('#desktop').length).toBe(1);
  });

  it('does not render mobile component', () => {
    const wrapper = mount(<Responsive />);
    expect(wrapper.find('#mobile').length).toBe(0);
  });
});