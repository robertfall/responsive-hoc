import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MediaQuery from 'react-responsive';

const MOBILE_BREAKPOINT = 768;

/* This is assuming only two breakpoints. It can become more sophisticated */
export default ({ Mobile, Desktop, Wrapper = ReactDOM.div }) =>
  class Responsive extends Component {
    render() {
      return (
        <div>
          <MediaQuery query={`(max-width: ${MOBILE_BREAKPOINT - 1}px)`}>
            {matches =>
              matches ? (
                <Mobile {...this.props} />
              ) : (
                <Desktop {...this.props} />
              )}
          </MediaQuery>
        </div>
      );
    }
  };
